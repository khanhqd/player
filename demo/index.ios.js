
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PanResponder,
  LayoutAnimation,
  Dimensions
} from 'react-native';
import Video from 'react-native-video';
var { height, width } = Dimensions.get('window');
import Slider from "react-native-slider";
import Orientation from 'react-native-orientation';
import Cookie from 'react-native-cookie';

export default class panresponder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pause: false,
      mute: false,
      seekableDuration: 0,
      currentTime: 0,
    }
  }

  componentDidMount() {
    Orientation.addOrientationListener(this._orientationDidChange);
    Cookie.set('https://r3---sn-npoe7n76.c.youtube.com', 'cookie', 'VISITOR_INFO1_LIVE=T3j5qTbzhoI').then(() => console.log('success'));
  }

  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      this.player.presentFullscreenPlayer()
    } else {

    }
  }

  componentWillUnmount() {
    Orientation.removeOrientationListener(this._orientationDidChange);
  }

  setTime = (a) => {
    // console.log(a)
    this.setState({
      currentTime: a.currentTime,
      seekableDuration: a.seekableDuration
    })
  }

  setDuration = (a) => {
    console.log(a)
  }

  loadStart = (a) => {
    console.log(a)
  }

  onEnd = (a) => {
    console.log(a)
  }
  videoError = (a) => {
    console.log(a)
  }

  onBuffer = (a) => {
    console.log(a)
  }

  onTimedMetadata = (a) => {
    console.log(a)
  }

  onSlidingComplete = () => {
    this.player.seek(this.state.currentTime)
    this.setState({
      pause: false
    })
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Video source={{ uri: "https://r3---sn-npoe7n76.c.youtube.com/videoplayback?mime=video%2Fmp4&gcr=vn&source=youtube&sparams=cp%2Cdur%2Cei%2Cgcr%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpcm2%2Cpl%2Cratebypass%2Crequiressl%2Csc%2Csource%2Cexpire&itag=22&expire=1502485557&initcwndbps=1113750&ratebypass=yes&pl=21&signature=9CEFD96881086DA237C3184849D9D4F0EC50E31F.797234AC3F95A6B58D05D713D77A48CF1BF3D6B1&requiressl=yes&key=yt6&ip=115.79.136.180&ipbits=0&pcm2=yes&ei=1ceNWaKJJ4LT4gKcgYzgCA&id=o-ADa63_E6rV-3qCJPkXEm-YcTG-xzLVPwAqW1qvYeKr8B&cp=U0pPR1RTU19KUUNQOV9MTVdGOlcyazNvVGZhaXBM&mm=30&mn=sn-npoe7n76&dur=8470.593&mt=1502463848&mv=m&sc=yes&ms=nxu&lmt=1499154269374642&ir=1&rr=12" }}   // Can be a URL or a local file.
          ref={(ref) => {
            this.player = ref
          }}                                      // Store reference
          rate={1}                              // 0 is paused, 1 is normal.
          volume={1}                            // 0 is muted, 1 is normal.
          muted={this.state.mute}                           // Mutes the audio entirely.
          paused={this.state.pause}                          // Pauses playback entirely.
          resizeMode="cover"                      // Fill the whole screen at aspect ratio.*
          repeat={false}                           // Repeat forever.
          playInBackground={false}                // Audio continues to play when app entering background.
          playWhenInactive={false}                // [iOS] Video continues to play when control or notification center are shown.
          ignoreSilentSwitch={"ignore"}           // [iOS] ignore | obey - When 'ignore', audio will still play with the iOS hard silent switch set to silent. When 'obey', audio will toggle with the switch. When not specified, will inherit audio settings as usual.
          progressUpdateInterval={500.0}          // [iOS] Interval to fire onProgress (default to ~250ms)
          onLoadStart={this.loadStart}            // Callback when video starts to load
          onLoad={this.setDuration}               // Callback when video loads
          onProgress={this.setTime}               // Callback every ~250ms with currentTime
          onEnd={this.onEnd}                      // Callback when playback finishes
          onError={this.videoError}               // Callback when video cannot be loaded
          onBuffer={this.onBuffer}                // Callback when remote video is buffering
          onTimedMetadata={this.onTimedMetadata}  // Callback when the stream receive some metadata 
          style={styles.backgroundVideo} />
        <View style={{ position: 'absolute', top: width / 16 * 9, left: 0, right: 0 }}>
          <Slider
            minimumValue={0}
            maximumValue={this.state.seekableDuration}
            onSlidingComplete={this.onSlidingComplete}
            onSlidingStart={() => {
              console.log('onslidingstart');
              this.setState({ pause: true })
            }}
            onValueChange={(value) => this.setState({ currentTime: value })}
            value={this.state.currentTime} />
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', position: 'absolute', top: width / 16 * 9 + 30, left: 0, right: 0, padding: 20 }}>
          <View onTouchStart={() => this.setState({ pause: !this.state.pause })}>
            <Text>
              Play/pause
            </Text>
          </View>
          <View onTouchStart={() => this.setState({ mute: !this.state.mute })}>
            <Text>
              Mute
            </Text>
          </View>
          <View onTouchStart={() => this.player.presentFullscreenPlayer()}>
            <Text>
              Fullscreen
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: width / 16 * 9,
    right: 0,
  }
});

AppRegistry.registerComponent('demo', () => panresponder);
