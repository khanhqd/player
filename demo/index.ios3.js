
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PanResponder
} from 'react-native';

export default class panresponder extends Component {
  constructor(props) {
    super(props);
    this.state={
      dx: 50,
      dy: 50,
      functionName: '',
      locationX: '',
      locationY: '',
      pageX: '',
      pageY: '',
      stateMoveX: '',
      statex0: '',
      statedx: '',
      statevx: ''
    }
    //Khởi tạo
    this.responder = PanResponder.create({
      // 2 hàm để check điều kiện và bắt đầu lắng nghe sự kiện
      // onStartShouldSetPanResponder: (e, gestureState) => {
      //   console.log("onStartShouldSet- page X ");
      //   console.log(gestureState.pageX)
      //   return true
      // },
      onMoveShouldSetPanResponder: (e, gestureState) => {
        this.setState({
          functionName: 'onMoveShouldSetPanResponder',
          locationX: e.nativeEvent.locationX,
          locationY: e.nativeEvent.locationY,
          pageX: e.nativeEvent.pageX,
          pageY: e.nativeEvent.pageY,
          stateMoveX: gestureState.moveX,
          statex0: gestureState.x0,
          statedx: gestureState.dx,
          statevx: gestureState.vx
        })
        return true
      },
      // onPanResponderGrant: (e, gestureState) => {},
      onPanResponderMove: (e, gestureState) => {
        console.log(gestureState.dx)
        this.setState({
          functionName: 'onPanResponderMove',
          locationX: e.nativeEvent.locationX,
          locationY: e.nativeEvent.locationY,
          pageX: e.nativeEvent.pageX,
          pageY: e.nativeEvent.pageY,
          stateMoveX: gestureState.moveX,
          statex0: gestureState.x0,
          statedx: gestureState.dx,
          statevx: gestureState.vx,
          dx: e.nativeEvent.pageX,
          dy: e.nativeEvent.pageY
        })
      },
      onPanResponderRelease: (e, gestureState) => {
        this.setState({
          dx: 50,
          dy: 50
        })
      }
    })
  }
  //pageX: khoảng cách từ vị trí touch đến cạnh trái của view Root
  //dx: khoảng cách theo chiều ngang giữa điểm bắt đầu Touch và điểm hiện tại
  render() {
    return (
      <View style={{flex:1}}>
        <View style={styles.container}>
          <View
          {...this.responder.panHandlers}
          style={[styles.box,{left: this.state.dx-50, top: this.state.dy-50 , position: 'absolute'}]}>
          </View>
        </View>
        <View style={styles.consoleBox}>
          <Text>Tên hàm: {this.state.functionName}
          </Text>
          <Text style={{fontSize: 13}}>
          Native Event: {'\n'}
          locationX -tọa độ X của touch trong view hiện tại :{this.state.locationX}{'\n'}
          locationY -tọa độ Y của touch trong view hiện tại :{this.state.locationY}{'\n'}
          pageX -KC hiện tại đến cạnh TRÁI của view Root: {this.state.pageX}{'\n'}
          pageY -KC hiện tại đến cạnh TRÊN của view Root: {this.state.pageY}{'\n'} {'\n'}
          gestureState: {'\n'}
          moveX -vị trí touch đến cạnh TRÁI của màn hình: {this.state.stateMoveX}{'\n'}
          x0: -Tọa độ X của touch trên màn hình {this.state.statex0} {'\n'}
          dx -KC đến vị trí touch khởi tạo: {this.state.statedx}{'\n'}
          vx -Vận tốc theo chiều ngang: {this.state.statevx}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  consoleBox: {
    backgroundColor: '#dfdfdf',
    borderColor: 'grey',
    borderWidth: 1,
    flex: 1,
    paddingLeft: 5
  },
  box: {
    height: 100,
    width: 100,
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: 'steelblue'
  }
});

AppRegistry.registerComponent('demo', () => panresponder);
