
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PanResponder,
  Animated,
  Dimensions
} from 'react-native';
var { height, width } = Dimensions.get('window');

export default class panresponder extends Component {
  constructor(props) {
    super(props);
    this.state={
      dx: 0,
      rotateDeg: 0,
      left: new Animated.Value(width/10)
    }
    this.responder = PanResponder.create({
      onMoveShouldSetPanResponder: (e, gestureState) => {
        return true
      },
      onPanResponderGrant: (e, gestureState) => {},
      onPanResponderMove: (e, gestureState) => {
        this.state.left.setValue(width/10 + gestureState.dx)
        this.setState({
          rotateDeg: gestureState.dx/2,
          dx: gestureState.dx
        })
      },
      onPanResponderRelease: (e, gestureState) => {
        if (this.state.dx < -100) {
            Animated.timing(
              this.state.left,
              { toValue: -width, duration: 1000 }
            ).start();
        }
        if (this.state.dx > 100) {
          Animated.timing(
            this.state.left,
            { toValue: width+50, duration: 1000 }
          ).start();
        }
        if ((this.state.dx < 100)&&(this.state.dx > -100)) {
          Animated.timing(
            this.state.left,
            { toValue: width/10, duration: 1000 }
          ).start();
        }
        setTimeout(()=>{
          Animated.timing(
            this.state.left,
            { toValue: width/10, duration: 1000 }
          ).start();
          this.setState({rotateDeg: 0})
        },2000)
      }
    })
  }
  render() {
    let rotateValue = this.state.left.interpolate({
      inputRange: [width/10,width/4,width/2],
      outputRange: ['0deg','45deg','90deg']
    })
    return (
      <View style={{flex:1}}>
        <View style={styles.container}>
          <Animated.View
          {...this.responder.panHandlers}
          style={[styles.box,{left: this.state.left,transform: [{ rotate: rotateValue }]}]}>
          </Animated.View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  consoleBox: {
    backgroundColor: '#dfdfdf',
    borderColor: 'grey',
    borderWidth: 1,
    flex: 1,
    paddingLeft: 5
  },
  box: {
    height: '50%',
    width: '80%',
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: 'steelblue',
    position: 'absolute'
  }
});

AppRegistry.registerComponent('demo', () => panresponder);
