
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PanResponder,
  LayoutAnimation
} from 'react-native';
import SafariView from "react-native-safari-view";
var SQLite = require('react-native-sqlite-storage')

export default class panresponder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      left: 50
    }
  }
  errorCB(err) {
    console.log("SQL Error: " + err);
  }

  successCB() {
    console.log("SQL executed fine");
  }

  openCB() {
    console.log("Database OPENED");
  }
  componentDidMount() {
    var db = SQLite.openDatabase("test.db", "1.0", "Test Database", 200000, this.openCB, this.errorCB);
    db.transaction((tx) => {
    tx.executeSql('SELECT * FROM Employees a, Departments b WHERE a.department = b.department_id', [], (tx, results) => {
      console.log("Query completed");

      // Get rows with Web SQL Database spec compliance.

      var len = results.rows.length;
      for (let i = 0; i < len; i++) {
        let row = results.rows.item(i);
        console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`);
      }

      // Alternatively, you can use the non-standard raw method.

      /*
        let rows = results.rows.raw(); // shallow copy of rows Array

        rows.map(row => console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`));
      */
    });
  });
  }
// onPressBtn() {
//   SafariView.isAvailable()
//     .then(SafariView.show({
//       url: "http://www.tinmoi24.vn/can-canh-yamaha-mt-09-tracer-moi-gia-274-trieu-dong/news-56-10-8a7f86d2210a8a6b8dee78e0a8864daa",
//       readerMode: true,
//       tintColor: "black",
//       barTintColor: "white",
//     }))
//     .catch(error => {
//       // Fallback WebView code for iOS 8 and earlier
//     });
// }
render() {
  return (
    <View style={{ flex: 1 }}>
      <View style={styles.container}>
        <View
          style={[styles.box, { left: this.state.dx, top: 50, position: 'absolute' }]}>
        </View>
      </View>
    </View>
  );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  consoleBox: {
    backgroundColor: '#dfdfdf',
    borderColor: 'grey',
    borderWidth: 1,
    flex: 1,
    paddingLeft: 5
  },
  box: {
    height: 100,
    width: 100,
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: 'steelblue'
  }
});

AppRegistry.registerComponent('demo', () => panresponder);
